#!/bin/bash
RC_VERSION=0.1
############################################################
# Help                                                     #
############################################################
Help()
{
   # Display Help
   echo "Run CI/CD pipeline command."
   echo
   echo "Syntax: run-cicd.sh [-g|h|v|V]"
   echo "options:"
   echo "h     Print this Help."
   echo "v     Verbose mode."
   echo "V     Print software version and exit."
   echo
}

############################################################
############################################################
# Main program                                             #
############################################################
############################################################
############################################################
# Process the input options. Add options as needed.        #
############################################################
# Get the options
while getopts ":h" option; do
   case $option in
      h) # display Help
         Help
         exit;;
   esac
done

option=$1
repo_path=$2
env=$3
rc_user=$4
rc_password=$5
# No input
if [[ -z $option ]]; then
   echo "run-cicd.sh: invalid option or no input "
   echo "syntax: ./run-cicd.sh [option] [repository path] [environment] [user] [password]"
   exit 1
fi
if [[ "$option" = "help" || "$option" = "Help" ]]; then
   Help
   exit 1
fi

pid=$$
this_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
return_code=0

echo this_path=$this_path

## Functions
# Array Search Index
get_arrayid() {
   local arg_val1=$1
   shift
   local arg_arr1=("$@")
   #echo "!arg_arr1[@]="${!arg_arr1[@]}
   #echo "#arg_arr1[@]="${#arg_arr1[@]}
   #echo "arg_arr1="${arg_arr1[@]}
   ind=-1
   for i in "${!arg_arr1[@]}"; do
      if [[ "${arg_arr1[$i]}" = "${arg_val1}" ]]; then
         ind="${i}"
      fi
   done
   echo $ind
}

# Read Repository Config
db_cicd_tool=$( grep "DB_CICD_TOOL" $repo_path/cfg/CICD.cfg | awk '{print $2}' )
echo db_cicd_tool=$db_cicd_tool

# Enable Library Path
. $this_path/lib/init-path.sh $db_cicd_tool

if [ "$db_cicd_tool" = "flyway" ]; then
   # Get Configuration Parameters
   flyway_path=$repo_path/$( grep "^FLYWAY_PATH" $repo_path/cfg/CICD.cfg | awk '{print $2}' )
   flyway_conf=$( grep "^FLYWAY_CONF" $repo_path/cfg/CICD.cfg | awk '{print $2}' )
   db_platform=$( grep "^DB_PLATFORM" $repo_path/cfg/CICD.cfg | awk '{print $2}' | tr '[A-Z]' '[a-z]' )
   db_platform_arr=($( grep "^DB_PLATFORMS" $repo_path/cfg/CICD.cfg | awk '{print $2}' |tr "," "\n" ))
   db_env_arr=($( grep "^DB_ENVS" $repo_path/cfg/CICD.cfg | awk '{print $2}' |tr "," "\n" ))
   db_name_arr=($( grep "^DB_NAMES" $repo_path/cfg/CICD.cfg | awk '{print $2}' | tr "," "\n" ))
   db_host_arr=($( grep "^DB_HOSTS" $repo_path/cfg/CICD.cfg | awk '{print $2}' | tr "," "\n" ))
   db_port_arr=($( grep "^DB_PORTS" $repo_path/cfg/CICD.cfg | awk '{print $2}' | tr "," "\n" ))

   #echo "!db_env_arr[@]="${!db_env_arr[@]}
   #echo "#db_env_arr[@]="${#db_env_arr[@]}
   #echo "db_env_arr="${db_env_arr[@]}
   #echo get_arrayid $db_env_arr $env
   #get_arrayid $env "${db_env_arr[@]}"
   db_env_ind=$( get_arrayid $env "${db_env_arr[@]}" )
   if [ $db_env_ind -eq -1 ]; then
      echo "DB_ENV not found!"
      exit -1
   fi
   #echo "db_env_ind=$db_env_ind db_name_arr[0]=${db_name_arr[0]}"
   db_name=${db_name_arr[$db_env_ind]}
   db_host=${db_host_arr[$db_env_ind]}
   db_port=${db_port_arr[$db_env_ind]}
   # Find Database Environment Value
   if [[ ! -z "$db_platform_arr" ]] || [[ ! "$db_platform_arr" = "" ]]; then
      db_platform=${db_platform_arr[$db_env_ind]}
   fi
   echo db_platform=$db_platform db_name=$db_name db_host=$db_host db_port=$db_port
   if [[ "$db_platform" = "sqlserver" ]]; then
      echo "MSSQL detected"
      jdbc_url="jdbc:$db_platform://$db_host:$db_port;databaseName=$db_name"
   else
      jdbc_url="jdbc:$db_platform://$db_host:$db_port/$db_name"
   fi
   #echo FLYWAY_PATH=$flyway_path
   echo FLYWAY_CONF=$flyway_conf
   # Calling Flyway 
   cd $repo_path/$flyway_path
   echo " ...calling flyway..."
   if [ "$option" = "deploy" ]; then
      echo flyway migrate -configFiles=$flyway_conf -url="$jdbc_url" -user="$rc_user" -password="*************"
      flyway info    -configFiles=$flyway_conf -url="$jdbc_url" -user="$rc_user" -password="$rc_password" > /tmp/$pid.flyway.out
      pending_count=$( grep "Pending" /tmp/$pid.flyway.out | wc -l | awk '{print $1}' )
      error_count=$( grep "ERROR:\|\SQL State\|Error Code" /tmp/$pid.flyway.out | wc -l | awk '{print $1}' )
      if [ $error_count -gt 0 ]; then
         return_code=1
      elif [ $pending_count -gt 0 ]; then
         flyway migrate -configFiles=$flyway_conf -url="$jdbc_url" -user="$rc_user" -password="$rc_password" | tee /tmp/$pid.flyway.out
         success_ind=$( grep "Successfully applied" /tmp/$pid.flyway.out | wc -l | awk '{print $1}' )
         if [ $success_ind -eq 1 ]; then
            return_code=0
         else
            return_code=1
         fi
      else
         return_code=255
      fi
      flyway info    -configFiles=$flyway_conf -url="$jdbc_url" -user="$rc_user" -password="$rc_password" 
      rm -f /tmp/$pid.flyway.out
   elif [ "$option" = "check" ]; then
      echo flyway info -configFiles=$flyway_conf -url="$jdbc_url" -user="$rc_user" -password="*************"
      flyway info    -configFiles=$flyway_conf -url="$jdbc_url" -user="$rc_user" -password="$rc_password" | tee /tmp/$pid.flyway.out
      pending_count=$( grep "Pending" /tmp/$pid.flyway.out | wc -l | awk '{print $1}' )
      error_count=$( grep "ERROR:\|\SQL State\|Error Code" /tmp/$pid.flyway.out | wc -l | awk '{print $1}' )
      if [ $error_count -gt 0 ]; then
         return_code=1
      elif [ $pending_count -gt 0 ]; then
         return_code=0
      elif [ $pending_count -eq 0 ]; then
         return_code=255
      fi
      rm -f /tmp/$pid.flyway.out
   elif [ "$option" = "info" ]; then
      echo flyway info -configFiles=$flyway_conf -url="$jdbc_url" -user="$rc_user" -password="*************"
      flyway info    -configFiles=$flyway_conf -url="$jdbc_url" -user="$rc_user" -password="$rc_password" | tee /tmp/$pid.flyway.out
      return_code=$?
      rm -f /tmp/$pid.flyway.out
   fi

   # Return Code Based on Output
   cd $this_path

   # Exit with $return_code
   exit $return_code

elif [ "$db_cicd_tool" = "liquibase" ]; then
   echo  "# Liquibase"
   liquibase -version
else 
   echo "# Flyway" 
   flyway -version
fi

