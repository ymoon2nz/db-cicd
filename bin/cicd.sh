#!/bin/bash

rc_repo_path=$1
rc_conf_file=$2
rc_platform=$3
rc_host=$4
rc_port=$5
rc_db=$6
rc_user=$7
rc_password=$8

cd $rc_repo_path

flyway -configFiles=$rc_conf_file -url="jdbc:$rc_platform://$rc_host:$rc_port/$rc_db" -user=$rc_user -password=$rc_password migrate
flyway -configFiles=$rc_conf_file -url="jdbc:$rc_platform://$rc_host:$rc_port/$rc_db" -user=$rc_user -password=$rc_password info

