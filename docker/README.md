
Docker for CICD
-------

* init Network
```
docker network create local-net
```

## Docker compose

## Db2 Docker
```
docker run --cpus 2 -itd --name db2 --network local-net --privileged=true -p 50000:50000 -e LICENSE=accept -e DB2INST1_PASSWORD=passDb2 ibmcom/db2 
```

## MSSQL Docker
```
docker run --cpus 2 -d --name=sqlserver --network local-net -e "MSSQL_PID=Express" -e ACCEPT_EULA=Y -e SA_PASSWORD=sql4Admin -p 1433:1433 -d mcr.microsoft.com/mssql/server:latest 
```

## Postgres Docker
```
docker run --cpus 1 --name postgres --network local-net -p 5432:5432 -e POSTGRES_PASSWORD=admin -d postgres
docker logs postgres -f

# Create Database
docker exec -it postgres psql -U postgres -c "CREATE DATABASE docker;"
docker exec -it postgres psql -U postgres -d docker -c "CREATE SCHEMA TEST1;"
```

## Oracle Docker
```
docker run --cpus 3 --name=oracle --network local-net -d -p 49161:1521 \
 -e ORACLE_ALLOW_REMOTE=true -e ORACLE_ENABLE_XDB=true -e ORACLE_ENABLE_XDB=true \
 oracleinanutshell/oracle-xe-11g 
```

## Jenkins Docker 
* Build packaged docker image
```
docker build . -t ymoon.xyz/jenkins-db
```

* Standard Run
```
# Mapping to local storage
docker run --cpus=2 --memory=4g --name jenkins-db \
  --network local-net -p 8080:8080 -p 40000:50000 \
  -v jenkins_home:/var/jenkins_home \
  -d ymoon.xyz/jenkins-db 

# No local storage mapping
docker run --cpus=2 --memory=4g --name jenkins-db \
  --network local-net -p 8080:8080 -p 40000:50000 \
  -d ymoon.xyz/jenkins-db 

# View Docker install key
docker logs jenkins-db -f

# User=jenkins
# Password=admin
```



