#!/bin/bash

plugins_file=$1
user=$2
pass=$3
host_n_port=$4

if [[ -z "$host_n_port" ]]; then
    host_n_port=localhost:8080
fi

mkdir ~/working
wget http://localhost:8080/jnlpJars/jenkins-cli.jar -P ~/working

while read -u 9 line
do
    echo java -jar ~/working/jenkins-cli.jar -s http://"$host_n_port"/ -auth $user:$pass install-plugin $line
    java -jar ~/working/jenkins-cli.jar -s http://"$host_n_port"/ -auth $user:$pass install-plugin $line
    echo "--- $line COMPLETED ---"
done 9< "$plugins_file"

rm -f ~/working/jenkins-cli.jar
