#!/bin/bash 

currPath=$(pwd)
#dirPath=$(dirname "$0")

thisPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo thisPath=$thisPath

## Add PATH Only Once
pathadd() {
   if [ -d "$1" ] && [[ ":$PATH:" != *":$1:"* ]]; then
      export PATH="${PATH:+"$PATH:"}$1"
   fi
}

## Add Flyway to PATH
addFlyway() {
   # Add flyway into PATH
   flywayDir=$(ls -l $thisPath | grep flyway | awk '{print $9}')
   flywayPath=$thisPath/$flywayDir
   if [ -d "$flywayPath" ] && [[ ":$PATH:" != *":$flywayPath:"* ]]; then
      export PATH="${PATH:+"$PATH:"}$flywayPath"
   fi
   #pathadd $thisPath/$flywayDir
}

# Add lib into PATH
   addFlyway

